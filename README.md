# Daily Arch Linux package of Firefox nightly

[![pipeline status](https://gitlab.com/archbuild/packages/firefox-nightly/badges/master/pipeline.svg)](https://gitlab.com/archbuild/packages/firefox-nightly/commits/master)

>
PKGBUILD taken from AUR https://aur.archlinux.org/packages/firefox-nightly/

## Install

1. [Download latest artifact](https://gitlab.com/archbuild/packages/firefox-nightly/-/jobs/artifacts/master/download?job=package)
1. Unzip it
1. Install with: `sudo pacman -U firefox-nightly-*.xz`

In one go:

```sh
wget -O /tmp/firefox.zip "https://gitlab.com/archbuild/packages/firefox-nightly/-/jobs/artifacts/master/download?job=package"
unzip /tmp/firefox.zip -d /tmp
sudo pacman --noconfirm --needed -U /tmp/firefox-nightly-*.xz
rm /tmp/firefox-nightly-*.xz /tmp/firefox.zip
```

Or use the [`ffup`](/ffup) script.

## Sync with AUR

To sync with the AUR package, run the [`aursync`](./aursync) script.
